﻿# Assignment

**Description**
-----
 * Project consists of 2 packages as the following:
     * test : This contains 2classes, 1 class to locators and methods that deals with Google play and the second class contains the test scenario.
     * utility: This contains the logging class.
  

**How to run?**
----
 * Run by right click on the ‘WebDriverTest’class and select run as testNG test. 
 * Run the ‘testSuite.xml’ as testNG suite. [Here we can select any browser to run test by , but I didn’t handle it]
 * If complier display error 'Failed to execute goal' or 'Failed to execute class path' please remove all files in .m2/reprository , restart Eclipse, update Maven with selecting force , Ran as clean Maven, Run as Maven Install, Clean project then Run project.
 * Please change add your google mail and password in the script before run 

The solution include: not completed yet
* Logging using log4j; 
* Report will be in the test-output folder after running the test;

**Libraries/Plugins used**
-------------------
* Selenium: To initiate the driver and deal with the web elements.
* Log4j: Used for logging.
* Testng: Used for test annotations , asserting on the results and parallel execution. 
* Maven-surefire-plugin: used for configuring the suite xml and parametrised variables through the command line.





