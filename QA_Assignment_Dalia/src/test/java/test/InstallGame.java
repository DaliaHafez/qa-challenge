package test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Log;

public class InstallGame {

    WebDriver driver;

    public InstallGame(WebDriver driver) {
        this.driver = driver;
    }

    public void installGame() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        Thread.sleep(10000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='My wishlist']")));
        WebElement wishlist = driver.findElement(By.xpath("//span[text()='My wishlist']"));
        wishlist.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='My wishlist']")));
        WebElement wishlistPage = driver.findElement(By.xpath("//span[text()='My wishlist']"));
        Log.info("System displayed  " + wishlistPage.getText());

        WebElement selectGame = driver.findElement(By.className("card-click-target"));
        selectGame.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='Added to wishlist']")));
        WebElement gameAdded = driver.findElement(By.xpath("//span[text()='Added to wishlist']"));
        Log.info("System opens game page correclty and displayed  " + gameAdded.getText());

        WebElement InstallButton = driver.findElement(By.className("oocvOe"));
        InstallButton.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("zHQkBf")));
        WebElement password = driver.findElement(By.className("zHQkBf"));
        password.clear();
        password.sendKeys(AddGameToWishList.passwordUsed);
        WebElement next = driver.findElement(By.cssSelector("span.RveJvd.snByac"));
        next.click();

        // check device and select what you want to install
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("contents")));

        Thread.sleep(8000);
        WebElement selectDevic = driver.findElement(By.className("device-selector"));
        selectDevic.getText();
        selectDevic.click();
        selectDevic.sendKeys("HUAWEI HUAWEI TIT-U02");

        WebElement install = driver.findElement(By.id("purchase-ok-button"));
        install.click();
        
        //Wait until system display onther pop-up that game start install
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("close-dialog-button")));
        WebElement okButton = driver.findElement(By.id("close-dialog-button"));
        okButton.click();
        Log.info("System start install game on device");
    }
}
