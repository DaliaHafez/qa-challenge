package test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Log;

public class WriteAandDeleteReview {
    WebDriver driver;

    public WriteAandDeleteReview(WebDriver driver) {
        this.driver = driver;
    }

    public void writeAndDeleteReview() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 50);
        Thread.sleep(3000);
        driver.navigate().to("https://play.google.com/store/apps/details?id=com.turbochilli.rollingsky");

        ///We should wait until write review button appeared
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("hwKISe")));
        WebElement insllatedButton = driver.findElement(By.className("hwKISe"));
        if(insllatedButton.isDisplayed()) {
            Log.info("Game already installed and 'Installed' button is displayed");
            WebElement writeReviewButton = driver.findElement(By.cssSelector("button.LkLjZd.ScJHi.HPiPcc.id-track-click"));
            Log.info(writeReviewButton.getText());
            writeReviewButton.click();
            //write-review-panel
            //Boolean popUpConfirmation=driver.findElement(By.cssSelector("div.review-help.id-gpr-onboard")).isDisplayed();
        /*    if(driver.findElement(By.cssSelector("div.review-help.id-gpr-onboard")).isDisplayed()) {
            WebElement continueButton = driver.findElement(By.cssSelector("button.play-button.id-enable-gpr.apps"));
            continueButton.click();
            }*/
            Thread.sleep(10000);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("write-review-comment-container")));
            Log.info("System located pop-up to write review");
            WebElement selectStars = driver.findElement(By.className("fifth-star"));
            Log.info("-------------> :D system locate stars");
            selectStars.click();
            
            WebElement writereview = driver.findElement(By.className("write-review-comment-container"));
            writereview.sendKeys("Good");
            
            WebElement submitReview = driver.findElement(By.className("id-submit-review"));
            submitReview.click();
            
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("xvpTFf")));
            WebElement deleteButton = driver.findElement(By.cssSelector("span.DPvwYc.NE4Eeb"));
            deleteButton.click();
        }
    }
}
