package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import utility.Log;

public class WebDriverTest {
 WebDriver driver;

  @BeforeTest
  public void startTesting() {
      System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver.exe");
      driver = new ChromeDriver();
      Log.info("Test is running successfully");
      driver.get("https://play.google.com");
      Log.info("System navigate to Google store");

        
  }
  

  @Test (priority=1, enabled = true)
  public void addGameToWishList() throws Exception {
      AddGameToWishList addgameTowishlist = new AddGameToWishList(driver);
      addgameTowishlist.vlaidAdd();
  }
  
  @Test (priority=2, enabled = true)
  public void installGame() throws Exception {
      InstallGame installFirstGame = new InstallGame(driver);
      installFirstGame.installGame();
  }
  
  @Test (priority=3, enabled = false)
  public void writeAndDeleteReview() throws Exception {
      WriteAandDeleteReview writeThenDeleteReview = new WriteAandDeleteReview(driver);
      writeThenDeleteReview.writeAndDeleteReview();
  }

  
  @AfterTest
  public void close() {
      driver.quit();
  }
}